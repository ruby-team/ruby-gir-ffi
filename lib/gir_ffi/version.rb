# frozen_string_literal: true

# Current GirFFI version
module GirFFI
  VERSION = "0.16.1"
end
